const { NotFoundError, LogicError } = require("../../common/errors");

module.exports =
  ({ securityService, ApiToken, UsersManage, validate }) =>
  async (payload) => {
    const { accountId } = payload;

    validate(payload, {
      accountId: "required|integer",
    });

    const usersManage = await UsersManage.findByPk(accountId);

    if (!usersManage) {
      throw new NotFoundError("User does not exist");
    }

    const apiToken = await ApiToken.findBy({ accountId });

    if (apiToken) {
      throw new LogicError("This account alrerady as a key");
    }

    const publicToken = securityService.generateToken({
      accountId,
      type: "publicToken",
    });

    await ApiToken.build({ accountId, publicToken }).save();

    return {
      publicToken,
    };
  };
