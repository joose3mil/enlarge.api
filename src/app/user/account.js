const { UnauthorizedError } = require("../../common/errors");
const { RoleToString } = require("../../common/roles");

module.exports =
  ({ securityService, UsersManage, validate }) =>
  async (headers) => {
    validate(headers, {
      "x-api-key": "required|string|min:10|max:255",
    });

    const { accountId, type } = securityService.decodeToken(
      headers["x-api-key"]
    );

    if (type !== "publicKey") {
      throw new UnauthorizedError("A public token must be given");
    }

    const {
      dataValues: { user_role, elg_img, renew_on, limit_dat },
    } = await UsersManage.findOne({
      where: { user_id: accountId },
      attributes: ["user_role", "elg_img", "renew_on", "limit_dat"],
    });

    return {
      role: RoleToString(user_role),
      remainingImages: elg_img,
      renovationDate: renew_on,
      planExpirationDate: limit_dat,
    };
  };
