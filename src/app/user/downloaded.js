const { UnauthorizedError } = require("../../common/errors");

module.exports =
  ({ securityService, EnlargedImages, validate }) =>
  async (headers) => {
    validate(headers, {
      "x-api-key": "required|string|min:10|max:255",
    });
    const { accountId, type } = securityService.decodeToken(headers["x-api-key"]);

    if (type !== "publicKey") {
      throw new UnauthorizedError("A public token must be given");
    }

    const enlargedImages = await EnlargedImages.findAll({
      raw: true,
      attributes: ["image_name", "date_en"],
      where: { user_manages_id: accountId },
    });

    return enlargedImages.map((enlargedImage) => {
      const { image_name, date_en } = enlargedImage;
      return {
        imageUrl: image_name,
        createAt: date_en,
      };
    });
  };
