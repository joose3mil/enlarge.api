const generateKey = require("./generateKey");
const downloaded = require("./downloaded");
const account = require("./account");

const { securityService, validatorService } = require("./../../services");

const { ApiToken, UsersManage, EnlargedImages } = require("./../../db/models");

module.exports = {
  generateKey: generateKey({
    securityService,
    ApiToken,
    UsersManage,
    validate: validatorService.validateForm,
  }),
  
  downloaded: downloaded({
    securityService, 
    EnlargedImages, 
    validate: validatorService.validateForm,
  }),

  account: account({
    securityService,
    UsersManage,
    validate: validatorService.validateForm,
  }),
};
