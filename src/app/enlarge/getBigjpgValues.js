const {aNoiseReduction, aImageType, aUpscaling} = require("../../common/values/apiValues");
const {bNoiseReduction, bImageType, bUpscaling} = require("../../common/values/bigJpgValues");

module.exports = (upscaling, noiseReduction,imageType) => {
    const payload = [upscaling, noiseReduction ,imageType].map(function(x){
        if (x===aUpscaling.X2){
            return ["scaling",bUpscaling.X2];
            
        }
        else if (x===aUpscaling.X4){
            return ["scaling",bUpscaling.X4];
            
        }
        else if (x===aUpscaling.X8){
            return ["scaling",bUpscaling.X8];
            
        }
        else if (x===aUpscaling.X16){
            return ["scaling",bUpscaling.X16];
            
        }
        else if (x===aNoiseReduction.None){
            return ["noise",bNoiseReduction.None];
            
        }
        else if (x===aNoiseReduction.Low){
            return ["noise",bNoiseReduction.Low];
            
        }
        else if (x===aNoiseReduction.Medium){
            return ["noise",bNoiseReduction.Medium];
            
        }
        else if (x===aNoiseReduction.High){
            return ["noise",bNoiseReduction.High];
            
        }
        else if (x===aNoiseReduction.Highest){
            return ["noise",bNoiseReduction.Highest];
            
        }
        else if (x===aImageType.Photo){
            return ["style",bImageType.Photo];
            
        }
        else if (x===aImageType.Artwork){
            return ["style",bImageType.Artwork];
            
        }

    });

     return Object.fromEntries(payload);

  };
  