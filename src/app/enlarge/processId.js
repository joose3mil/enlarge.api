const saveImage = require("./saveImage");

module.exports = ({validate,axios, securityService,getUserId, path }) =>
  async (payload) => {
    console.log(payload);
    validate(payload, {
      "x-api-key": "required|string|min:10|max:255",
      processId: "required|string",       
    });
    const { accountId, type } = await securityService.decodeToken(
      payload["x-api-key"]
    );
    const userId = await getUserId(accountId);
    const res = await axios.get('https://bigjpg.com/api/task/'+payload.processId)
    .then(function (response) {
      // handle success
      if (response.data[payload.processId].status=== "error"){
        return {status:"failed" }; 
      }
      if (response.data[payload.processId].status=== "success"){
        const baseName = path.basename(response.data[payload.processId].url);
        const a = saveImage(accountId, userId, response.data[payload.processId].url, baseName);
      }
      return response.data[payload.processId];
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })


    return res;
  };