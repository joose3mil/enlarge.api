const { UsersManage } = require("../../db/models");

module.exports =async (accountId) => {
    const {elg_img} = await UsersManage.findOne({
        where: { user_id: accountId },
        attributes: [ "elg_img"],
      });
      await UsersManage.update(
          {elg_img: elg_img-1},
          { where: { user_id: accountId } }
          )
    return elg_img-1;

}