const { LogicError } = require("../../common/errors");
const {aNoiseReduction, aImageType, aUpscaling} = require("../../common/values/apiValues");
const discountImage = require("./discountImage");

module.exports =
  ({validate, axios,validateAccount, FormData,securityService,RoleToString, getTokenFromRole,getBigjpgValues }) =>
  async ( payload) => {
    validate(payload, {
      "x-api-key": "required|string|min:10|max:255",
      image: "required|url",
      upscaling: `required|in:${Object.entries(aUpscaling)}`,
      imageType: `required|in:${Object.entries(aImageType)}`,
      noiseReduction: `required|in:${Object.entries(aNoiseReduction)}`,       
    });

    const { accountId, type } = securityService.decodeToken(
      payload["x-api-key"]
    );

    await validateAccount(accountId, type);

    const key = await getTokenFromRole(RoleToString(accountId));

    const {style,noise,scaling}=getBigjpgValues(payload.upscaling, payload.noiseReduction,payload.imageType);

    const form = new FormData();
    const config =`{"style": "${style}", "noise": "${noise}", "x2":"${scaling}", "input": "${payload.image}"}`;
      
      form.append('conf',config );
      const conType=form.getHeaders();
      const headers={... conType,"x-api-key":key}
      const res = await axios.post('https://bigjpg.com/api/task/', form, {
        headers
      });

      if (res.status !==200|| res.data.status==="parallel_limit"){
        throw new LogicError( "Something was wrong, please try it later");
      }

     await discountImage(accountId);

    return {processId: res.data.tid, estimatedTime: res.data.minute};
  };