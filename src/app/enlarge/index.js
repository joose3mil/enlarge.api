const start = require("./start");
const processId = require("./processId");
const { securityService, validatorService } = require("./../../services");
const axios = require('axios');
const https = require('https');
const FormData = require('form-data');
const { RoleToString, getTokenFromRole } = require("../../common/roles");
const  getBigjpgValues   = require("./getBigjpgValues");
const  validateAccount  = require("./validateAccount");
const getUserId = require("../../common/getUserId");
const path = require('path');

module.exports = {
  start: start({
    securityService,
    validate: validatorService.validateForm,
    axios,
    FormData,
    RoleToString,
    getTokenFromRole,
    getBigjpgValues,
    validateAccount,
  }),
  processId: processId({
    axios,
    FormData,
    securityService,
    getUserId,
    path,
    validate: validatorService.validateForm,
  }),
};