const { UsersManage } = require("../../db/models");
const { UnauthorizedError } = require("../../common/errors");
module.exports = async (accountId, type) => {

   if (type !== "publicKey") {
     throw new UnauthorizedError("A public token must be given");
   }

    const {
        dataValues: {  elg_img, limit_dat },
      } = await UsersManage.findOne({
        where: { user_id: accountId },
        attributes: [ "elg_img", "limit_dat"],
      });

       if (elg_img<1){
        throw new UnauthorizedError("This account has reached its image-limit. Upgrate your plan to continue enlarging ");
      }

      const date = new Date();
      if (date>limit_dat){
        throw new UnauthorizedError("Your plan has explired");
      }
  };