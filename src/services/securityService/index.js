const decodeToken = require('./decodeToken');
const generateToken = require('./generateToken');
const isEqual = require('./isEqual');
const encrypt = require('./encrypt');

module.exports = {
  decodeToken,
  generateToken,
  isEqual,
  encrypt,
};
