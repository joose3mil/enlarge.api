const jsonwebtoken = require("jsonwebtoken");
const { config } = require("../../common");

const SECRET_KEY = config.get("app.key");

module.exports = (payload) => {
  return jsonwebtoken.sign(payload, SECRET_KEY);
};
