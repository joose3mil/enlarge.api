const getFirstError = require('./getFirstError');
const getFirstErrorByField = require('./getFirstErrorByField');
const getValidatedData = require('./getValidatedData');

module.exports = {
  getFirstError,
  getFirstErrorByField,
  getValidatedData,
};
