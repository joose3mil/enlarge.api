const Validator = require('validatorjs');
const uniqueField = require('./customRules/uniqueField');
const afterOrEqualTo = require('./customRules/afterOrEqualTo');
const intPhone = require('./customRules/intPhone');
const countryAlpha2 = require('./customRules/countryAlpha2');
const usaStateCode = require('./customRules/usaStateCode');
const prMunicipality = require('./customRules/prMunicipality');
const isAdult = require('./customRules/isAdult');
const messagesDefault = require('./messagesDefault.json');

Validator.useLang('es');

Validator.setMessages('es', Object.assign(Validator.getMessages('es'), messagesDefault));

// Custom rules
Validator.registerAsync('unique', uniqueField, 'Ya existe');

Validator.register(
  afterOrEqualTo.RULE_NAME,
  afterOrEqualTo,
  afterOrEqualTo.DEFAULT_VALIDATION_MESSAGE,
);

Validator.register(intPhone.RULE_NAME, intPhone, intPhone.DEFAULT_VALIDATION_MESSAGE);

Validator.register(
  countryAlpha2.RULE_NAME,
  countryAlpha2,
  countryAlpha2.DEFAULT_VALIDATION_MESSAGE,
);

Validator.register(usaStateCode.RULE_NAME, usaStateCode, usaStateCode.DEFAULT_VALIDATION_MESSAGE);

Validator.register(
  prMunicipality.RULE_NAME,
  prMunicipality,
  prMunicipality.DEFAULT_VALIDATION_MESSAGE,
);

Validator.register(isAdult.RULE_NAME, isAdult, isAdult.DEFAULT_VALIDATION_MESSAGE);

module.exports = Validator;
