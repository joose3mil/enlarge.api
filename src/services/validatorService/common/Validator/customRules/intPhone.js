const parsePhone = require('libphonenumber-js');

const RULE_NAME = 'intPhone';
const DEFAULT_VALIDATION_MESSAGE = 'Invalid phone number';

/**
 * Checks if a given string is a valid international phone number.
 *
 * @param {string} value phone
 */
function intPhone(value) {
  const phone = parsePhone(value);
  return phone ? phone.isValid() : false;
}

module.exports = intPhone;
module.exports.RULE_NAME = RULE_NAME;
module.exports.DEFAULT_VALIDATION_MESSAGE = DEFAULT_VALIDATION_MESSAGE;
