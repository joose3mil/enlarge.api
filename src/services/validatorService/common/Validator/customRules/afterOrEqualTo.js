// Based on: https://github.com/skaterdav85/validatorjs/issues/279#issuecomment-559046269

const RULE_NAME = 'after_or_equal_to';
const DEFAULT_VALIDATION_MESSAGE = `:attribute debe ser igual o mayor a :${RULE_NAME}`;

const isValid = (date) => !Number.isNaN(date);

/**
 * Checks if a given date is after or equal to other given date.
 *
 * @param {string} value input date
 * @param {string} param date to compare from
 */
function afterOrEqualTo(value, param) {
  const inputDate = new Date(value);
  const afterDate = new Date(param);

  if (!isValid(inputDate) || !isValid(afterDate)) {
    return false;
  }

  return inputDate.getTime() >= afterDate.getTime();
}

module.exports = afterOrEqualTo;
module.exports.RULE_NAME = RULE_NAME;
module.exports.DEFAULT_VALIDATION_MESSAGE = DEFAULT_VALIDATION_MESSAGE;
