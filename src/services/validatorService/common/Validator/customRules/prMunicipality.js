const RULE_NAME = 'prMunicipality';
const DEFAULT_VALIDATION_MESSAGE = 'Invalid country';

/**
 * Puerto Rico municipalities codes.
 */
const codesSet = new Set([
  'PR-SJ',
  'PR-AB',
  'PR-BY',
  'PR-GB',
  'PR-CT',
  'PR-TB',
  'PR-TA',
  'PR-NR',
  'PR-CZ',
  'PR-DO',
  'PR-VA',
  'PR-VB',
  'PR-MT',
  'PR-CL',
  'PR-MV',
  'PR-FL',
  'PR-BC',
  'PR-AC',
  'PR-HA',
  'PR-CA',
  'PR-QB',
  'PR-AL',
  'PR-IS',
  'PR-AD',
  'PR-MC',
  'PR-SS',
  'PR-RC',
  'PR-AN',
  'PR-MG',
  'PR-MR',
  'PR-LM',
  'PR-HO',
  'PR-CR',
  'PR-SG',
  'PR-LJ',
  'PR-PO',
  'PR-PN',
  'PR-GL',
  'PR-YU',
  'PR-SB',
  'PR-GC',
  'PR-UT',
  'PR-AJ',
  'PR-LR',
  'PR-JY',
  'PR-OR',
  'PR-BQ',
  'PR-CM',
  'PR-VL',
  'PR-CY',
  'PR-CD',
  'PR-CO',
  'PR-AI',
  'PR-GM',
  'PR-SA',
  'PR-AR',
  'PR-JD',
  'PR-SI',
  'PR-CG',
  'PR-GR',
  'PR-JC',
  'PR-SL',
  'PR-HU',
  'PR-NG',
  'PR-LP',
  'PR-YB',
  'PR-MB',
  'PR-PT',
  'PR-FJ',
  'PR-RG',
  'PR-LQ',
  'PR-CB',
  'PR-CU',
  'PR-VQ',
  'PR-CN',
  'PR-TJ',
  'PR-LZ',
  'PR-CV',
]);

/**
 * Checks if a given string is a valid PR municipality code
 *
 * @param {string} value input date
 */
function prMunicipality(value) {
  return codesSet.has(value);
}

module.exports = prMunicipality;
module.exports.RULE_NAME = RULE_NAME;
module.exports.DEFAULT_VALIDATION_MESSAGE = DEFAULT_VALIDATION_MESSAGE;
