const RULE_NAME = 'usaStateCode';
const DEFAULT_VALIDATION_MESSAGE = 'Invalid country';

/**
 * United States Postal Codes.
 * Data taken from: https://www.infoplease.com/us/postal-information/state-abbreviations-and-state-postal-codes
 */
const codesSet = new Set([
  'AK',
  'AL',
  'AR',
  'AS',
  'AZ',
  'CA',
  'CO',
  'CT',
  'DC',
  'DE',
  'FL',
  'FM',
  'GA',
  'GU',
  'HI',
  'IA',
  'ID',
  'IL',
  'IN',
  'KS',
  'KY',
  'LA',
  'MA',
  'MD',
  'ME',
  'MH',
  'MI',
  'MN',
  'MO',
  'MP',
  'MS',
  'MT',
  'NC',
  'ND',
  'NE',
  'NH',
  'NJ',
  'NM',
  'NV',
  'NY',
  'OH',
  'OK',
  'OR',
  'PA',
  'PR',
  'PW',
  'RI',
  'SC',
  'SD',
  'TN',
  'TX',
  'UT',
  'VA',
  'VI',
  'VT',
  'WA',
  'WI',
  'WV',
  'WY',
]);

/**
 * Checks if a given string is a valid US postal code
 *
 * @param {string} value input date
 */
function useStateCode(value) {
  return codesSet.has(value);
}

module.exports = useStateCode;
module.exports.RULE_NAME = RULE_NAME;
module.exports.DEFAULT_VALIDATION_MESSAGE = DEFAULT_VALIDATION_MESSAGE;
