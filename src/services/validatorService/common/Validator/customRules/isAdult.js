/* eslint-disable no-param-reassign */

const moment = require('moment');

const RULE_NAME = 'isAdult';
const DEFAULT_VALIDATION_MESSAGE = 'Invalid date of birth';

/**
 * @param {string} value date string
 */
function isAdult(value) {
  try {
    value = moment(value);
  } catch (error) {
    return false;
  }
  const now = moment();
  return now.diff(value, 'years') >= 18;
}

module.exports = isAdult;
module.exports.RULE_NAME = RULE_NAME;
module.exports.DEFAULT_VALIDATION_MESSAGE = DEFAULT_VALIDATION_MESSAGE;
