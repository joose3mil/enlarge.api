const { ValidationError } = require('../../common/errors');
const { getFirstErrorByField, getValidatedData } = require('./common/helpers');
const { Validator } = require('./common');

const getValidatorError = (validation) =>
  new ValidationError('Campos no válidos', getFirstErrorByField(validation));

function validate(data, rules, messages = {}) {
  const validation = new Validator(data, rules, messages);

  if (validation.hasAsync) {
    return new Promise((resolve, reject) => {
      validation.checkAsync(
        () => resolve(getValidatedData(validation)),
        () => reject(getValidatorError(validation)),
      );
    });
  }

  if (validation.fails()) {
    throw getValidatorError(validation);
  }

  return getValidatedData(validation);
}

module.exports = validate;
