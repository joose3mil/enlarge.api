const randomstring = require('randomstring');
const dateService = require('../dateService');

module.exports = () => {
  return `${dateService.create().format('YYMMDDHHmmss')}${randomstring.generate({
    length: 5,
    readable: true,
    charset: 'alphabetic',
    capitalization: 'uppercase',
  })}`;
};
