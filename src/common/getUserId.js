const {UsersManage } = require("./../db/models");
module.exports = async (accountId) => {
    const {id} = await UsersManage.findOne({
        where: { user_id: accountId },
        attributes: [ "id"],
      });
      return id;

}