const Roles = require("./Roles");
const RoleToString = require("./RoleToString");
const getTokenFromRole = require("./getTokenFromRole");

module.exports = {
  Roles,
  RoleToString,
  getTokenFromRole,
};
