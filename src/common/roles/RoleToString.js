const Roles = require("./Roles");

module.exports = (role) => {
  let roleString;
  if (role === 0) {
    roleString = Roles.FREE;
  } else if (role === 1) {
    roleString = Roles.STANDARD;
  } else if (role === 2) {
    roleString = Roles.PRO;
  } else {
    return "Invalid role";
  }
  return roleString;
};
