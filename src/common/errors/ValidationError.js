class ValidationError extends Error {
  constructor(message, errors = undefined) {
    super(message);
    this.errors = errors;
    this.name = this.constructor.name;
    this.isAntdError = false;
    this.antdField = null;
  }

  getErrors() {
    if (this.isAntdError) {
      return {
        [this.antdField]: this.message,
      };
    }
    return this.errors;
  }

  toJSON() {
    return {
      message: this.message,
      errors: this.errors,
    };
  }

  static asAntd(field, message) {
    const error = new ValidationError(message);
    error.antdField = field;
    error.isAntdError = true;
    return error;
  }
}

module.exports = ValidationError;
