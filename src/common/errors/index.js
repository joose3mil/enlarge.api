const LogicError = require('./LogicError');
const NotFoundError = require('./NotFoundError');
const ValidationError = require('./ValidationError');
const UnauthorizedError = require('./UnauthorizedError');
const BadRequest = require('./BadRequestError');

module.exports = {
  LogicError,
  NotFoundError,
  ValidationError,
  UnauthorizedError,
  BadRequest,
};
