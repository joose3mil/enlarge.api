const config = require("./config");
const errors = require("./errors");

module.exports = {
  config,
  errors,
};
