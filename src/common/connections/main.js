const Sequelize = require('sequelize');
const cls = require('cls-hooked');
const config = require('../config');

const dbConfig = config.get('db');
const timezone = config.get('app.timezone');

const namespace = cls.createNamespace('defaultnmspc');
Sequelize.useCLS(namespace);

module.exports = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
  host: dbConfig.host,
  port: dbConfig.port,
  dialect: 'mysql',
  timezone,
  // eslint-disable-next-line no-console
  logging: dbConfig.log ? console.log : () => {},
});
