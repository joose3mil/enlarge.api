require("dotenv").config();

const configuration = {
  app: {
    key: process.env.APP_KEY,
    timezone: process.env.TZ,
  },
  api: {
    port: process.env.PORT,
    url: process.env.API_URL,
    nodeEnv: process.env.NODE_ENV,
  },
  db: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT || 3306,
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    log: process.env.DB_LOG.toUpperCase() !== "FALSE",
  },
};

module.exports = configuration;
