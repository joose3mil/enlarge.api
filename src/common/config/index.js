const settings = require('./settings');
const config = require('./config');

module.exports = config(settings);
