const config = (configuration) => ({
  get: (key) => {
    const [first, second] = key.split('.');

    return second ? configuration[first][second] : configuration[first];
  },
});

module.exports = config;
