const { config } = require("../common");

const api = require("./api");
const API_PORT = config.get("api.port");

// eslint-disable-next-line no-console
api.listen(API_PORT, () => console.log(`Listening on port ${API_PORT}`));
