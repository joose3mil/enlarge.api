const statusCodes = require('./statusCodes');
const asyncHandler = require('./asyncHandler');

module.exports = {
  asyncHandler,
  statusCodes,
};
