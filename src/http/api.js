const bodyParser = require('body-parser');
const express = require('./common/express');
const { cors, notFoundToResponse, errorToResponse } = require('./middleware');

const app = express();

// parse JSON requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors);

// Load routes
app.use(require('./routes'));

app.use(notFoundToResponse);

app.use(errorToResponse);

module.exports = app;
