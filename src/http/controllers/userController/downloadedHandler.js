const { asyncHandler } = require("../../common");
const  {downloaded}  = require("../../../app/user");

async function downloadedHandler(req, res) {
  return res.json( await downloaded(req.headers));
}
module.exports = asyncHandler(downloadedHandler);
