const router = require("express").Router();
router.post("/generateKey", require("./generateKeyHandler")); 
router.get("/downloaded", require("./downloadedHandler"));
router.get("/account", require("./accountHandler"));
module.exports = router;
