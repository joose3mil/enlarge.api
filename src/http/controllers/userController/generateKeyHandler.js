const { asyncHandler } = require("../../common");
const { generateKey } = require("../../../app/user");

async function generateKeyHandler(req, res) {
  return res.json(await generateKey(req.body));
}
module.exports = asyncHandler(generateKeyHandler);
