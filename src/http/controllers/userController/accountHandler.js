const { asyncHandler } = require("../../common");
const { account } = require("../../../app/user");

async function accountHandler(req, res) {
  return res.json(await account(req.headers));
}

module.exports = asyncHandler(accountHandler);
