const router = require("express").Router();
router.post("/start", require("./startHandler"));
router.get("/processId", require("./processIdHandler")); 
module.exports = router;
