const { asyncHandler } = require("../../common");
const  {processId}  = require("../../../app/enlarge");
async function processIdHandler(req, res) {
  return res.json( await processId({...req.headers, ...req.body})) 

}
module.exports = asyncHandler(processIdHandler);