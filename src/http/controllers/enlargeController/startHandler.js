const { asyncHandler } = require("../../common");
const  {start}  = require("../../../app/enlarge");
async function startHandler(req, res) {
  return res.json( await start({...req.headers, ...req.body})); 

}
module.exports = asyncHandler(startHandler);