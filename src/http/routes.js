const router = require("express").Router();

router.get("/", (req, res) => res.json({ app: "hola mundo", isUp: true }));

router.use("/user", require("./controllers/userController"));
router.use("/enlarge", require("./controllers/enlargeController"));

module.exports = router;
