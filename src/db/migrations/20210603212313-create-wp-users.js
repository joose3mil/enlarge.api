"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("wp_users", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      user_login: {
        type: Sequelize.STRING,
      },
      users_pass: {
        type: Sequelize.STRING,
      },
      user_nicename: {
        type: Sequelize.STRING,
      },
      user_email: {
        type: Sequelize.STRING,
      },
      user_url: {
        type: Sequelize.STRING,
      },
      user_activation_key: {
        type: Sequelize.STRING,
      },
      user_status: {
        type: Sequelize.INTEGER,
      },
      display_name: {
        type: Sequelize.STRING,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("wp_users");
  },
};
