"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable("api_token", {
      id: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.BIGINT.UNSIGNED,
      },
      accountId: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      privateToken: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      publicToken: {
        allowNull: false,
        type: Sequelize.STRING,
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("api_token");
  },
};
