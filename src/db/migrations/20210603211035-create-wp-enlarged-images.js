"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "wp_enlarged_images",
      {
        nro: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        user_id: {
          type: Sequelize.BIGINT,
        },
        image_name: {
          type: Sequelize.STRING,
        },
        down_name: {
          type: Sequelize.STRING,
        },
        date_en: {
          type: Sequelize.DATE,
        },
        user_manages_id: {
          type: Sequelize.INTEGER,
        },
      },
      { define: { freezeTableName: true } }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("wp_enlarged_images");
  },
};
