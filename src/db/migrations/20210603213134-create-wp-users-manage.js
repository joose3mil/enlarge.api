"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("wp_users_manage", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      ip: {
        type: Sequelize.STRING,
      },
      elg_img: {
        type: Sequelize.INTEGER,
      },
      sign_dat: {
        type: Sequelize.DATE,
      },
      limit_dat: {
        type: Sequelize.DATE,
      },
      user_role: {
        type: Sequelize.INTEGER,
      },
      renew_on: {
        type: Sequelize.DATE,
      },
      email_is_chekd: {
        type: Sequelize.INTEGER,
      },
      token_id: {
        type: Sequelize.INTEGER,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("wp_users_manage");
  },
};
