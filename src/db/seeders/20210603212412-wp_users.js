"use strict";

module.exports = {
  up: async (queryInterface) => {
    // Encrypted value: admin
    const password =
      "$2b$10$caZKqRkU/yPizGU8Gc2coeWDPPHJ3Dou4BukWhGvKwo/5e5EUV782";

    await queryInterface.bulkInsert("wp_users", [
      {
        id: 1,
        user_email: "joose3mil@gmail.com",
        users_pass: password,
      },
    ]);
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete("wp_users", null);
  },
};
