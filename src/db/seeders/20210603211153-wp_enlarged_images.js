module.exports = {
  up: async (queryInterface) => {

    await queryInterface.bulkInsert("wp_enlarged_images", [
      {
        user_id: 1,
        image_name: "/directorio/imagenes/imagen.jpg",
        down_name: "imagen.jpg",
        date_en: new Date("2020/01/20"),
        user_manages_id: 1,
      },
    ]);
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete("wp_enlarged_images", null);
  },
};