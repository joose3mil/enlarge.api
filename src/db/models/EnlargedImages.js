const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class EnlargedImages extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  EnlargedImages.init(
    {
      nro: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.BIGINT.UNSIGNED,
      },
      user_id: DataTypes.BIGINT,
      image_name: DataTypes.STRING,
      down_name: DataTypes.STRING,
      date_en: DataTypes.DATE,
      user_manages_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "wp_enlarged_images",
      timestamps: false,
    }
  );
  return EnlargedImages;
};
