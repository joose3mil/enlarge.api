const sequelize = require("../../common/connections/main");

const User = require("./Users");
const ApiToken = require("./ApiToken");
const EnlargedImages = require("./EnlargedImages");
const UsersManage = require("./UsersManage");

const models = {
  User: User(sequelize),
  ApiToken: ApiToken(sequelize),
  EnlargedImages: EnlargedImages(sequelize),
  UsersManage: UsersManage(sequelize),
};

// register model's associations
Object.values(models).forEach((model) => {
  if (model.associate) {
    model.associate(models);
  }
});

module.exports = models;
