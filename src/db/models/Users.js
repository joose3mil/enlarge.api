const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Users.init(
    {
      id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.BIGINT.UNSIGNED,
      },
      user_login: DataTypes.STRING,
      users_pass: DataTypes.STRING,
      user_nicename: DataTypes.STRING,
      user_email: DataTypes.STRING,
      user_url: DataTypes.STRING,
      user_activation_key: DataTypes.STRING,
      user_status: DataTypes.INTEGER,
      display_name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "wp_users",
      tableName: "wp_users",
      timestamps: false,
    }
  );
  return Users;
};
