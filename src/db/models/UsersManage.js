const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class UsersManage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  UsersManage.init(
    {
      id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.BIGINT.UNSIGNED,
      },
      user_id: DataTypes.INTEGER,
      ip: DataTypes.STRING,
      elg_img: DataTypes.INTEGER,
      sign_dat: DataTypes.DATE,
      limit_dat: DataTypes.DATE,
      user_role: DataTypes.INTEGER,
      renew_on: DataTypes.DATE,
      email_is_chekd: DataTypes.INTEGER,
      token_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "wp_users_manage",
      tableName: "wp_users_manage",
      timestamps: false,
    }
  );
  return UsersManage;
};
