const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class ApiToken extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ApiToken.init(
    {
      id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.BIGINT.UNSIGNED,
      },
      accountId: DataTypes.BIGINT,
      privateToken: DataTypes.STRING,
      publicToken: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "api_token",
      tableName: "api_token",
      timestamps: false,
    }
  );
  return ApiToken;
};
